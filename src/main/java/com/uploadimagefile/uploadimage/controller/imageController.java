package com.uploadimagefile.uploadimage.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.uploadimagefile.uploadimage.entity.imageEntity;
import com.uploadimagefile.uploadimage.model.imageModel;
import com.uploadimagefile.uploadimage.model.response.ResponseImage;
import com.uploadimagefile.uploadimage.model.response.ResponseModel;
import com.uploadimagefile.uploadimage.model.response.ServiceResult;
import com.uploadimagefile.uploadimage.service.stotageSerivce;

@Controller
@RestController
public class imageController {

	@Autowired
	private stotageSerivce stotageSerivce;

	@PostMapping("/upload")
	public ResponseModel<imageModel> upload(@RequestParam("file") MultipartFile file) {
		ResponseModel<imageModel> responseModel = new ResponseModel<>();
		if (file.getName() != null
				&& file.getContentType() != null
				&& file.getOriginalFilename() != null) {
			String fireName = StringUtils.cleanPath(file.getOriginalFilename());
			try {
				System.out.println(file.getOriginalFilename());
				System.out.println(file.getBytes());
				imageEntity copyparam = new imageEntity();
				imageModel param = new imageModel(fireName, file.getContentType(), file.getBytes());
				copyparam.setName(param.getName());
				copyparam.setType(param.getType());
				copyparam.setData(param.getData());
				ServiceResult<imageEntity> serviceResult = stotageSerivce.upload(copyparam);
				if (serviceResult.isSuccess()) {
					imageEntity result = serviceResult.getData();
					imageModel model = new imageModel(result);
					responseModel.setData(model);
					responseModel.setMessages("Upload Success");
				}
			} catch (IOException e) {
				System.out.print(e.getMessage());
			}
		} else {
			responseModel.setMessages("No Success");
		}
		return responseModel;
	}

	@PostMapping("/findAll")
	public ResponseModel<List<ResponseImage>> allFind () {
		ResponseModel<List<ResponseImage>> responseModel = new ResponseModel<List<ResponseImage>>();
		List<ResponseImage> file = stotageSerivce.getAllFiles().map(imageEntity -> {
			String urlFile = ServletUriComponentsBuilder.fromCurrentContextPath().path("/image").toString();
			return new ResponseImage(imageEntity.getName(),urlFile,imageEntity.getType(),imageEntity.getData().length);
		}).collect(Collectors.toList());
		responseModel.setData(file);
		responseModel.setMessages("Success");
		return responseModel;
	}
	
}
