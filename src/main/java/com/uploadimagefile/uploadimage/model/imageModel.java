package com.uploadimagefile.uploadimage.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.uploadimagefile.uploadimage.entity.imageEntity;

@JsonInclude(Include.NON_NULL)
public class imageModel {

	@JsonProperty(value = "id")
	private Integer id;
	
	@JsonProperty(value = "name")
	private String name;
	
	@JsonProperty(value = "type")
	private String type;
	
	@JsonProperty(value = "data")
	private byte[] data;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	
	public imageModel() {
		
	}
	
	public imageModel(imageEntity entity) {
		this.id = entity.getId();
		this.name = entity.getName();
		this.type = entity.getType();
		this.data = entity.getData();
	}
	
	public imageModel(String name, String type, byte[] data) {
		this.name = name;
		this.type = type;
		this.data = data;
	}
}
