package com.uploadimagefile.uploadimage.model.response;

import java.io.Serializable;

public class ServiceResult <T extends Object> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public T data;
	public boolean success;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	public ServiceResult(T data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public ServiceResult() {
		
	}
}
