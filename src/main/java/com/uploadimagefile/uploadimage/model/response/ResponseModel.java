package com.uploadimagefile.uploadimage.model.response;

import java.io.Serializable;

public class ResponseModel <T extends Object> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public T data;
	public String messages;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	public ResponseModel(T data) {
		this.data = data;
	}
	
	public String getMessages() {
		return messages;
	}

	public void setMessages(String messages) {
		this.messages = messages;
	}

	public ResponseModel() {
		
	}
}
