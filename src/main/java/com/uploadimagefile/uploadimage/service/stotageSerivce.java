package com.uploadimagefile.uploadimage.service;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.uploadimagefile.uploadimage.entity.imageEntity;
import com.uploadimagefile.uploadimage.model.response.ServiceResult;
import com.uploadimagefile.uploadimage.repository.imageRepository;

@Service
public class stotageSerivce {

	@Autowired
	private imageRepository imageRepository;

	public ServiceResult<imageEntity> upload(imageEntity param) {
		ServiceResult<imageEntity> serivceResult = new ServiceResult<imageEntity>();
		imageEntity result = imageRepository.save(param);
		serivceResult.setData(result);
		serivceResult.setSuccess(true);
		return serivceResult;
	}

	public Stream<imageEntity> getAllFiles() {
		return imageRepository.findAll().stream();
	}
}
