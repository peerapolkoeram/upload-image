package com.uploadimagefile.uploadimage.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uploadimagefile.uploadimage.entity.imageEntity;

@Repository
public interface imageRepository extends JpaRepository<imageEntity, Integer> {

}
